#!/bin/bash

docker run -it --rm -v /data/fcc201908:/usr/src/fcc201908 \
    -v /data/maven_repo:/root/.m2/repository \
    -w /usr/src/fcc201908  maven:3.6.1-jdk-8 \
    mvn exec:java -Dexec.mainClass="fcc201908.TestMySQL"
#    mvn clean package exec:java -Dexec.mainClass="fcc201908.TestMySQL"
