package fcc201908;

import java.util.Random;
import java.sql.Connection;
import java.sql.Statement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import javax.sql.DataSource;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class TestMySQL implements Runnable {

    public static void main(String[] args) throws Exception {
        ApplicationContext applicationContext = new ClassPathXmlApplicationContext("beans.xml");
        DataSource dataSource = applicationContext.getBean("dataSource", DataSource.class);

        try (Connection connection = dataSource.getConnection()) {
            try (Statement statement = connection.createStatement()) {
                statement.executeUpdate("DELETE FROM t_test");
            }
        }

        int[][] userIds = getUserIds(2000, 10, 200);
        long beginTimestamp = System.currentTimeMillis();
        Thread[] threads = new Thread[200];
        for (int i = 0; i < threads.length; i += 1) {
            threads[i] = new Thread(new TestMySQL(i, userIds[i], dataSource.getConnection()));
            threads[i].start();
        }
        for (int i = 0; i < threads.length; i += 1) {
            threads[i].join();
        }
        System.out.println(System.currentTimeMillis() - beginTimestamp);
    }

    private static int[][] getUserIds(int userCount, int loopCount, int threadCount) {
        int[] buffer = new int[userCount * loopCount];
        for (int i = 0; i < userCount; i += 1) {
            for (int j = 0; j < loopCount; j += 1) {
                buffer[10 * i + j] = i;
            }
        }

        Random random = new Random();
        for (int i = buffer.length; i > 0; ) {
            int index = random.nextInt(i);
            i -= 1;
            if (index < i) {
                int temp = buffer[i];
                buffer[i] = buffer[index];
                buffer[index] = temp;
            }
        }

        int[][] result = new int[threadCount][buffer.length / threadCount];
        for (int i = buffer.length; i > 0; ) {
            i -= 1;
            result[i % threadCount][i / threadCount] = buffer[i];
        }
        return result;
    }

    private int threadCode;
    private int[] userIds;
    private Connection connection;

    public TestMySQL(int threadCode, int[] userIds, Connection connection) throws java.sql.SQLException {
        this.threadCode = threadCode;
        this.userIds = userIds;
        this.connection = connection;
        this.connection.setAutoCommit(false);
    }

    @Override public void run() {
        for (int i = 0; i < 100; i += 1) {
            System.out.println("Thread " + this.threadCode + ", loop " + i + ": begin");
            try {
                doWithMySQL(i);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
        try {
            this.connection.close();
            System.out.println("Thread " + this.threadCode + ": connection closed");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
    
    private void doWithMySQL(int i) throws Exception {
        try (Statement statement = this.connection.createStatement()) {
            statement.executeUpdate("LOCK TABLES t_test WRITE");
        }
        boolean canGet = true;
        try (PreparedStatement statement = this.connection.prepareStatement("SELECT user_id FROM t_test WHERE user_id = ?")) {
            statement.setInt(1, this.userIds[i]);
            try (ResultSet resultSet = statement.executeQuery()) {
                canGet = !resultSet.next();
            }
        }
        if (canGet) {
            try (Statement statement = this.connection.createStatement()) {
                try (ResultSet resultSet = statement.executeQuery("SELECT Count(*) c FROM t_test")) {
                    resultSet.next();
                    canGet = (resultSet.getInt("c") < 10);
                }
            }
        }
        if (canGet) {
            try (PreparedStatement statement = this.connection.prepareStatement("INSERT INTO t_test (user_id) VALUES (?)")) {
                statement.setInt(1, this.userIds[i]);
                statement.executeUpdate();
            }
        }
        try (Statement statement = this.connection.createStatement()) {
            statement.executeUpdate("UNLOCK TABLES");
        }
        this.connection.commit();
        System.out.println("Thread " + this.threadCode + ", loop " + i + ": committed");
    }
}
